// SPDX-FileCopyrightText: 2021 Jonah Brüchert <jbb@kaidan.im>
//
// SPDX-License-Identifier: LGPL-2.1-or-later

use serde::{Deserialize, Deserializer};

use std::fmt::Debug;

extern crate serde;

#[derive(Deserialize, Debug)]
enum Status {
    Actual,
}

#[derive(Deserialize, Debug)]
enum MsgType {
    Update,
    Alert,
    Cancel,
}

#[derive(Deserialize, Debug)]
enum Scope {
    Public,
}

#[derive(Deserialize, Debug)]
enum Urgency {
    Immediate,
}

#[derive(Deserialize, Debug)]
enum Severity {
    Minor,
    Severe,
    Extreme
}

#[derive(Deserialize, Debug)]
enum Certainty {
    Observed,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct NamedValue {
    value_name: String,
    value: String,
}

#[derive(Debug)]
struct Coordinate {
    x: f32,
    y: f32,
}

#[derive(Debug)]
struct Polygon(Vec<Coordinate>);

impl<'de> Deserialize<'de> for Polygon {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let buf = String::deserialize(deserializer)?;

        let mut coordinates = Vec::<Coordinate>::new();

        for coordinate in buf.split_whitespace() {
            let mut xy = coordinate.split(',');
            let x = xy.next();

            if let Some(x) = x {
                let y = xy.next();
                if let Some(y) = y {
                    coordinates.push(Coordinate {
                        x: x.parse::<f32>().map_err(serde::de::Error::custom)?,
                        y: y.parse::<f32>().map_err(serde::de::Error::custom)?,
                    })
                } else {
                    return Err(serde::de::Error::custom(
                        "Coordinate didn't have an y component",
                    ));
                }
            } else {
                return Err(serde::de::Error::custom(
                    "Coordinate didn't have an x component",
                ));
            }
        }

        Ok(Polygon(coordinates))
    }
}

/// example: DE-BY-A-W083-20200828-000
struct Identifier {
    country: String,
    state: String,
    county: String,
    date: String,
    sender_id: String,
    warning_number: i32,
}

impl<'de> Deserialize<'de> for Identifier {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let buf = String::deserialize(deserializer)?;

        let mut split = buf.split("-");

        let error = "Reference did not contain the correct number of parts";

        Ok(Identifier {
            country: split.next().expect(error).to_owned(),
            state: split.next().expect(error).to_owned(),
            county: split.next().expect(error).to_owned(),
            date: split.next().expect(error).to_owned(),
            sender_id: split.next().expect(error).to_owned(),
            warning_number: split.next().expect(error).parse::<i32>().expect("warning number was not a correct inteer")
        })
    }
}

impl ToString for Identifier {
    fn to_string(&self) -> String {
        return format!(
            "{}-{}-{}-{}-{}-{:0>3}",
            self.country, self.state, self.county, self.date, self.sender_id, self.warning_number
        );
    }
}

impl Debug for Identifier {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.to_string())
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Area {
    area_desc: String,
    polygon: Vec<Polygon>,
    geocode: Vec<NamedValue>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Info {
    language: String,
    category: Vec<String>,
    event: String,
    urgency: Urgency,
    severity: Severity,
    certainty: Certainty,
    event_code: Vec<NamedValue>,
    headline: String,
    description: String,
    instruction: Option<String>,
    web: Option<String>,
    contact: Option<String>,
    parameter: Vec<NamedValue>,
    area: Vec<Area>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
struct Message {
    identifier: Identifier,
    sender: String,
    sent: String,
    status: Status,
    msg_type: MsgType,
    scope: Scope,
    code: Vec<String>,
    references: Option<String>,
    info: Vec<Info>,
}

#[cfg(test)]
mod tests {
    use crate::Message;
    use std::{error::Error, fs::File};

    #[test]
    fn parse() -> Result<(), Box<dyn Error>> {
        let warnungen: Vec<Message> =
            serde_json::from_reader(File::open("gefahrendurchsagen.json")?)?;
        println!("{:#?}", warnungen);

        Ok(())
    }
}
